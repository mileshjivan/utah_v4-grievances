/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Grievances_TestSuite
{

    static TestMarshall instance;

    public S5_2_Grievances_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //FR1-Capture Grievances - Main Scenario
    @Test
    public void FR1_Capture_Grievances_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR1-Capture Grievances - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Grievances_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR1-Capture Grievances - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Grievances_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR1-Capture Grievances - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR1_Capture_Grievances_AlternateScenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR1-Capture Grievances - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2-Complete Registration - Main Scenario
    @Test
    public void FR2_Complete_Registration_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR2-Complete Registration - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR2_Complete_Registration_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR2-Complete Registration - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3-View Related Engagement - Main Scenario 
    @Test
    public void FR3_View_Related_Engagement_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR3-View Related Grievances - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR4-Capture Initial Response - Main Scenario
    @Test
    public void FR4_Capture_Initial_Response_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR4-Capture Initial Response - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR4_Capture_Initial_Response_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR4-Capture Initial Response - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR5-Capture Investigation - Main Scenario
    @Test
    public void FR5_Capture_Investigation_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR5-Capture Investigation - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR5_Capture_Investigation_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR5-Capture Investigation - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR6-Capture Action Plan - Main Scenario
    @Test
    public void FR6_Capture_Action_Plan_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR6-Capture Action Plan - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR6-Capture Action Plan - Alternate Scenario
    @Test
    public void FR6_Capture_Action_Plan_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR6-Capture Action Plan - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR7-Capture Final Responce - Main Scenario
    @Test
    public void FR7_Capture_Final_Response_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR7-Capture Final Response - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Final_Response_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR7-Capture Final Response - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR7_Capture_Final_Response_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR7-Capture Final Response - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Resolve_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR8-Capture Resolve - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Resolve_Alternate_Scenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR8-Capture Resolve - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Resolve_Alternate_Scenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR8-Capture Resolve - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    @Test
    public void FR8_Capture_Resolve_Alternate_Scenario3() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Grievances v5.2\\FR8-Capture Resolve - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
