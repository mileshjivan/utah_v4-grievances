/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Grievances - Alternate Scenario 2",
        createNewBrowserInstance = false
)

public class FR1_Capture_Grievances_AlternateScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;
    String fullName;
    String parentWindow;

    public FR1_Capture_Grievances_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
        fullName = getData("Known as") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setFullName(fullName);
        
    }

    public TestResult executeTest()
    {
        if (!navigateToGrievances())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureGrievances())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Stakeholder Individual -> Grievance' record.");
    }

    public boolean navigateToGrievances()
    {
        //Navigate to Social Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.navigate_socialSustainability()))
        {
            error = "Failed to wait for 'Social Sustainability' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.navigate_socialSustainability()))
        {
            error = "Failed to click on 'Social Sustainability' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'navigate_socialSustainability' button.");
        pause(1000);

        //Navigate to Complaints & Grievances
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.navigate_complaintsGrievances()))
        {
            error = "Failed to wait for 'Complaints & Grievances' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.navigate_complaintsGrievances()))
        {
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Grievances' serch page.");
        pause(2000);

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievance_add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievance_add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        pause(10000);
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean captureGrievances()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceProcess_flow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceProcess_flow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }

        //Grievance title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceTitle()))
        {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.grievanceTitle(), grievanceTitle))
        {
            error = "Failed to enter '" + grievanceTitle + "' into Grievance title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + grievanceTitle + "' into Grievance title field.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.BusinessUnitTab()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnitTab()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Summary description field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceSummary()))
        {
            error = "Failed to wait for 'Summary description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.grievanceSummary(), getData("Summary description")))
        {
            error = "Failed to enter '" + getData("Summary description") + "' into Summary description field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Summary description") + "' into Summary description field.");

        //Received by dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.receivedByDD()))
        {
            error = "Failed to wait for 'Received by' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.receivedByDD()))
        {
            error = "Failed to click on 'Received by' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Received by' dropdown.");
        //Received by select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.individualOrEntitySelect(getData("Received by"))))
        {
            error = "Failed to wait for '" + getData("Received by") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.individualOrEntitySelect(getData("Received by"))))
        {
            error = "Failed to click on '" + getData("Received by") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Received by") + "' option");

        //Individual/Entity dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.individualOrEntityDD()))
        {
            error = "Failed to wait for 'Individual/Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.individualOrEntityDD()))
        {
            error = "Failed to click on 'Individual/Entity' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Individual/Entity' dropdown.");
        //Individual/Entity select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.individualOrEntitySelect_2(getData("Individual/Entity"))))
        {
            error = "Failed to wait for '" + getData("Individual/Entity") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.individualOrEntitySelect_2(getData("Individual/Entity"))))
        {
            error = "Failed to click on '" + getData("Individual/Entity") + "' option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Individual/Entity") + "' option");

        //Reception date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.receptionDate()))
        {
            error = "Failed to wait for 'Receiption date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.receptionDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Receiption date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into Receiption date field.");

        //Capture new Stakeholder Individual
        CaptureStakeholderIndividual();

        //Grievant name dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievantNameDD()))
        {
            error = "Failed to wait for 'Grievant name' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievantNameDD()))
        {
            error = "Failed to click on 'Grievant name' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grieventNameSelect(Grievances_PageObject.getFullName())))
        {
            error = "Failed to wait for Grievant name option :" + getData("Grievant name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grieventNameSelect(Grievances_PageObject.getFullName())))
        {
            error = "Failed to click on Grievant name :" + getData("Grievant name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Grievant name") + "' option");

        //Grievance location 
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.GrievanceLocationTab()))
        {
            error = "Failed to wait for Grievance location dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceLocationTab()))
        {
            error = "Failed to click the Grievance location dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.BusinessUnit_Option(getData("Grievance location 1"))))
        {
            error = "Failed to wait for Grievance location Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option(getData("Grievance location 1"))))
        {
            error = "Failed to click the Grievance location Option: " + getData("Business Unit 1");
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 2"))))
//        {
//            error = "Failed to click the Grievance location Option: " + getData("Grievance location 2");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 3"))))
//        {
//            error = "Failed to click the Grievance location Option: " + getData("Business Unit 3");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option(getData("Grievance location 4"))))
//        {
//            error = "Failed to click the Grievance location Option: " + getData("Grievance location 4");
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Grievance location: " + getData("Grievance location 1") + " -> " + getData("Grievance location 2") + " -> " + getData("Grievance location 3") + " -> " + getData("Grievance location 4"));

        //Responsibility dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.ResponsibilityDD()))
        {
            error = "Failed to wait for 'Responsibility' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.ResponsibilityDD()))
        {
            error = "Failed to click on 'Responsibility' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.responsibilitySelect(getData("Responsibility"))))
        {
            error = "Failed to wait for Responsibility option :" + getData("Responsibility");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.responsibilitySelect(getData("Responsibility"))))
        {
            error = "Failed to click on Responsibility :" + getData("Responsibility");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsibility") + "' option");

        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn()))
        {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }

    public boolean CaptureStakeholderIndividual()
    {

        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

        //If grievant not registered ADD new
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.AddNewIndividual()))
        {
            error = "Failed wait for If grievant not registered ADD new button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.AddNewIndividual()))
        {
            error = "Failed to click If grievant not registered ADD new button.";
        }
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to tab";
            return false;
        }

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Grievances_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Process_Flow()))
        {
            error = "Failed to wait for process flow";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Process_Flow()))
        {
            error = "Failed to click process flow";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Process Flow");

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Stakeholder_First_Name()))
        {
            error = "Failed to wait for First name";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.Stakeholder_First_Name(), getData("First name")))
        {
            error = "Failed to enter First name";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.Stakeholder_Last_Name(), getData("Last name")))
        {
            error = "Failed to enter Last name";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.Stakeholder_Known_as(), Grievances_PageObject.getFullName()))
        {
            error = "Failed to enter Known as";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Title()))
        {
            error = "Failed to click title";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Grievances_Title_Option(getData("Title"))))
        {
            error = "Failed to wait for title option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievances_Title_Option(getData("Title"))))
        {
            error = "Failed to click title option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Status()))
        {
            error = "Failed to click status";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Grievances_Status_Option(getData("Status"))))
        {
            error = "Failed to wait for status option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievances_Status_Option(getData("Status"))))
        {
            error = "Failed to click status option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievances_Relationship_Owner()))
        {
            error = "Failed to click grievances relationship owner";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Grievances_Relationship_Owner_Option(getData("Relationship owner"))))
        {
            error = "Failed to wait for grievances relationship owneroption";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievances_Relationship_Owner_Option(getData("Relationship owner"))))
        {
            error = "Failed to click grievances relationship owner option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Stakeholder_Categories()))
        {
            error = "Failed to click stakeholder categories ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Stakeholder_Business_Units()))
        {
            error = "Failed to click stakeholder business units ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Stakeholder_Impact_Types()))
        {
            error = "Failed to click stakeholder impact types";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Button_Save()))
        {
            error = "Failed to click button save";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked save button.");

        if (SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.inspection_Record_Saved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.inspection_Record_Saved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.inspection_Record_Saved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Successfully capture Stakeholder Individual");

        pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Grievances_PageObject.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }

        return true;
    }
}
