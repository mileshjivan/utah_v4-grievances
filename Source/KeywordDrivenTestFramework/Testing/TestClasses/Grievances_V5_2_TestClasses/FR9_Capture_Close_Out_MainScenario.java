/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR9-Capture Close-Out v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR9_Capture_Close_Out_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR9_Capture_Close_Out_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!Capture_Close_Out()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Step 7: Close-Out record.");
    }

    public boolean Capture_Close_Out() {        
        //STEP 7: Close-out tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.STEP_7_Close_out_Tab())) {
            error = "Failed to wait for 'STEP 7: Close-out' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.STEP_7_Close_out_Tab())) {
            error = "Failed to click on 'STEP 7: Close-out' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'STEP 7: Close-out' tab");
        
        //Final response field
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.CommentsOnCloseOut(), getData("Comments on close-out"))) {
            error = "Failed to enter '" + getData("Comments on close-out") + "' into Comments on close-out field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Comments on close-out") + "' into Comments on close-out field.");
        
        //Has the close-up been completed? dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.closeUpBeenCompleted_Dropdown())) {
            error = "Failed to wait for 'Has the close-up been completed?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.closeUpBeenCompleted_Dropdown())) {
            error = "Failed to click on 'Has the close-up been completed?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.closeUpBeenCompleted_Option(getData("close-up been completed")))) {
            error = "Failed to wait for Has the close-up been completed? option :" + getData("close-up been completed");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.closeUpBeenCompleted_Option(getData("close-up been completed")))) {
            error = "Failed to click on Has the close-up been completed? option :" + getData("close-up been completed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Has the close-up been completed? Option: " + getData("close-up been completed"));
                
        //Save or save to continue button
        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Step7_SaveToContinue_SaveBtn())){
                error = "Failed to wait for Save to continue button";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Step7_SaveToContinue_SaveBtn())){
                error = "Failed to wait for Save to continue button";
                return false;
            }
        } else {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn())) {
                error = "Failed to wait for Save button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn())) {
                error = "Failed to wait for Save button";
                return false;
            }
        }
        
        pause(30000);
        
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }        
//        narrator.stepPassedWithScreenShot("Successfully clicked Save button");
//
//        //Validate if the record has been saved or not.
//        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave())){
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());
//
//        if (!SaveFloat.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        
        //Validate grievance status
        if(getData("close-up been completed").equalsIgnoreCase("Yes")){
            //Closed
            if(!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.GrievanceStatus())){
                error = "Failed to wait for 'Closed' status: " + getData("Status");
                return false;
            }
            if(!SeleniumDriverInstance.scrollToElement(Grievances_PageObject.GrievanceStatus())){
                error = "Failed to scroll to the 'Closed' status: " + getData("Status");
                return false;
            }
            if(!SeleniumDriverInstance.ValidateByText(Grievances_PageObject.GrievanceStatus(), getData("Status"))){
                error = "Failed to click the  'Closed' status: " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("'STEP 7: Close-out' status have successfully validated: " + getData("Status"));
        } else {
            //STEP 7: Close-out
            if(!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.GrievanceStatus())){
                error = "Failed to wait for 'STEP 7: Close-out' status: " + getData("Status");
                return false;
            }
            if(!SeleniumDriverInstance.scrollToElement(Grievances_PageObject.GrievanceStatus())){
                error = "Failed to scroll to the 'STEP 7: Close-out' status: " + getData("Status");
                return false;
            }
            if(!SeleniumDriverInstance.ValidateByText(Grievances_PageObject.GrievanceStatus(), getData("Status"))){
                error = "Failed to click the  'STEP 7: Close-out' status: " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("'STEP 7: Close-out' status have successfully validated: " + getData("Status"));
        }
            
        return true;
    }
}
