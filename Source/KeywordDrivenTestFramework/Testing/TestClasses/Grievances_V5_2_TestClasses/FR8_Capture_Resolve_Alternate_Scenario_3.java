/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR8 Capture Resolve - Alternate Scenario 3",
        createNewBrowserInstance = false
)
public class FR8_Capture_Resolve_Alternate_Scenario_3 extends BaseClass
{

    String error = "";
    String grievanceTitle;

    public FR8_Capture_Resolve_Alternate_Scenario_3()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Resolve())
        {
            return narrator.testFailed("Capture Resolve Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured resolve");
    }

    public boolean Capture_Resolve()
    {

        pause(3000);
        //right arrow 
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to wait for scroll right arrow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to click on scroll right arrow";
            return false;
        }

        //Navigate to STEP 6: Resolve tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Resolve_tab()))
        {
            error = "Failed to wait for STEP 6: Resolve tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Resolve_tab()))
        {
            error = "Failed to click on STEP 6: Resolve tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked to Resolve tab.");
        pause(2000);

        //Grievance outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Grievance_outcome()))
        {
            error = "Failed to wait for Grievance outcome drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievance_outcome()))
        {
            error = "Failed to click on Grievance outcome drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected  Grievance outcome drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Grievance outcome"))))
        {
            error = "Failed to wait for Grievance outcome drop down option :" + getData("Grievance outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Grievance outcome"))))
        {
            error = "Failed to click on Grievance outcome drop down option :" + getData("Grievance outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Grievance outcome :" + getData("Grievance outcome"));
        pause(2000);

        //Accept
        String Accept = getData("Grievance outcome");
        if (Accept.equalsIgnoreCase("Objection"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Accept()))
            {
                error = "Failed to wait for Accept / reject the objection? drop down.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Accept()))
            {
                error = "Failed to click on Accept / reject the objection? drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Accept / reject the objection? drop down.");
            pause(2000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Accept"))))
            {
                error = "Failed to wait for Accept / reject the objection? drop down option :" + getData("Accept");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Accept"))))
            {
                error = "Failed to click on Accept / reject the objection? drop down option :" + getData("Accept");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Accept / reject the objection? :" + getData("Accept"));
            pause(2000);
        }

        //Comments on outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Comments_on_outcome()))
        {
            error = "Failed to wait for Comments on outcome :" + getData("Comments on outcome");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.Comments_on_outcome(), getData("Comments on outcome")))
        {
            error = "Failed to enter Comments on outcome :" + getData("Comments on outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Comments on outcome:" + getData("Comments on outcome"));

        //Outcome received by
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Outcome_received_by()))
        {
            error = "Failed to wait for Outcome received by drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Outcome_received_by()))
        {
            error = "Failed to click on Outcome received by drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received by drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received by"))))
        {
            error = "Failed to wait for Outcome received by drop down option :" + getData("Outcome received by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received by"))))
        {
            error = "Failed to click on Outcome received by drop down option :" + getData("Grievance outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received by :" + getData("Grievance outcome"));
        pause(2000);

        //return
        //Outcome received
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Return()))
        {
            error = "Failed to wait for Outcome received drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Return()))
        {
            error = "Failed to click on return drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received"))))
        {
            error = "Failed to wait for Outcome received drop down option :" + getData("Outcome received");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received"))))
        {
            error = "Failed to click on Outcome received drop down option :" + getData("Outcome received");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked return :" + getData("Outcome received"));
        pause(2000);

        String outcome_data = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.Outcome_data());
        //validate auto populated date

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.SaveAndContinue_STEP7()))
        {
            error = "Failed to wait for Outcome received drop down option :" + getData("Outcome received");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.SaveAndContinue_STEP7()))
        {
            error = "Failed to click on Outcome received drop down option :" + getData("Outcome received");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked return :" + getData("Outcome received"));
        pause(2000);
        
        pause(30000);

        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.Step_1_Registration()))
        {
            error = "Failed to wait for STEP 1: Registration";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to click on scroll right arrow";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.supporting_tab()))
        {
            error = "Failed to click on support tab";
            return false;
        }
        pause(3000);
        //failing at this point 
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to click on scroll right arrow";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.supporting_tab()))
        {
            error = "Failed to click on support tab";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to click on scroll right arrow";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceHistory_Tab()))
        {
            error = "Failed to click on Grievance History Tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.GrievanceHistory_Gridview()))
        {
            error = "Failed to wait For Grievance History Gridview";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Navigated To Grievance History Tab");
        pause(300);

        String results = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.GrievanceHistory_Results());

        if (results.equalsIgnoreCase("No results returned"))
        {
            if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.left_arrow()))
            {
                error = "Failed to wait for scroll left arrow";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.left_arrow()))
            {
                error = "Failed to click on scroll left arrow";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.ActionPlan_Tab()))
            {
                error = "Failed to wait For action plan tab";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.ActionPlan_Tab()))
            {
                error = "Failed to click on  action plan tab";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.GrievanceActionPlan_Panel()))
            {
                error = "Failed to wait For Grievance Action Plan";
                return false;
            }

            String status = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.Grievance_Action_Status());

            narrator.stepPassedWithScreenShot("Successfully retrieve Grievance Action Status :" + status);
        }
        return true;
    }
}
