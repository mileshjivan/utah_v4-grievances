/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR7-Capture Final Response - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR7_Capture_Final_Response_AlternateScenario1 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR7_Capture_Final_Response_AlternateScenario1() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!captureFinalResponse()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Initial Response' record.");
    }

    public boolean captureFinalResponse() {
        
        //STEP 5: Final Response tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.finalRsponseTab())) {
            error = "Failed to wait for 'STEP 5: Final Response' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.finalRsponseTab())) {
            error = "Failed to click on 'STEP 5: Final Response' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'STEP 5: Final Response' tab");
        
        //Final response field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.finalResponse())) {
            error = "Failed to wait for 'Final response' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.finalResponse(), getData("Final response"))) {
            error = "Failed to enter '" + getData("Final response") + "' into Final response field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Final response") + "' into Final response field.");
        
        //Has the action plan been accepted? dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanAccepted())) {
            error = "Failed to wait for 'Has the action plan been accepted?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanAccepted())) {
            error = "Failed to click on 'Has the action plan been accepted?' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Has the action plan been accepted? dropdown");
        
        //Has the action plan been accepted? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanCompeted_Select(getData("Action plan accepted")))) {
            error = "Failed to wait for Has the action plan been accepted? option :" + getData("Action plan accepted");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanCompeted_Select(getData("Action plan accepted")))) {
            error = "Failed to click on Has the action plan been accepted? option :" + getData("Action plan accepted");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Action plan accepted") + "' option");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        
//        //Saving mask
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Successfully clicked Save button");
        
        pause(30000);
        
        //Navigate to Grievance history tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceHistory()))
        {
            error = "Failed to wait for Grievance history tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistory()))
        {
            error = "Failed to click on Grievance history tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Grievance history tab.");

        //Grievance history record
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceHistoryRecord()))
        {
            error = "Failed to wait for Grievance history record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistoryRecord()))
        {
            error = "Failed to click on Grievance history record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Grievance history record.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.grievanceHistory_ProcessFlow()))
        {
            error = "Failed to wait for Process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistory_ProcessFlow()))
        {
            error = "Failed to click on Process flow button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked process flow button");
        
        return true;
    }
}
