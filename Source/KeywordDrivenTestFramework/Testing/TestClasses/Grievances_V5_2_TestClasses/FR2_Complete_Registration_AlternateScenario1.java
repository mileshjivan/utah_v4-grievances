/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Complete Registration - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR2_Complete_Registration_AlternateScenario1 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR2_Complete_Registration_AlternateScenario1() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!completeRegistration()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Complete Registration' record.");
    }

    public boolean completeRegistration() {
                
        //Registration complete dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RegistrationComplete())) {
            error = "Failed to wait for 'Registration complete' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RegistrationComplete())) {
            error = "Failed to click on 'Registration complete' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Registration complete dropdown");
        //Registration complete select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Registration complete")))) {
            error = "Failed to wait for Registration complete option :" + getData("Registration complete");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Registration complete")))) {
            error = "Failed to click on Registration complete :" + getData("Registration complete");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Registration complete") + "' option");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        pause(25000);
        
//        //Validate if the record has been saved or not.
//        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave())){
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());
//
//        if (!SaveFloat.equals("Record saved")){
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
