/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SKhumalo
 */
public class Grievances_PageObject extends BaseClass
{

    static String fullName;
    static String grievanceTitle;
    static String projectTitle;
    static String commitmentTitle;

    public static String BusinessUnitHeader()
    {
        return "//div[text()='Business unit']";
    }

    public static String EngagementHeader()
    {
        return "//div[text()='Engagements']";
    }

    public static String Resolve_tab()
    {
        return "//div[text()='STEP 6: Resolve']";
    }

    public static String Step7_SaveToContinue_SaveBtn()
    {
        return "//div[@id='control_B16C0F2E-94CA-4080-855D-21196423458D']";
    }

    public static String CommentsOnCloseOut()
    {
        return "//div[@id='control_8E50592C-5FF4-4A2B-BD81-95789EC95993']//textarea";
    }

    public static String closeUpBeenCompleted_Dropdown()
    {
        return "//div[@id='control_CAF67324-9F1B-4A3D-91AF-05516EC6EF54']";
    }

    public static String closeUpBeenCompleted_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Grievance_outcome()
    {
        return "//div[@id='control_CD1B93DB-29B8-4A0A-A476-DD5B0C2E6DF8']//ul";
    }

    public static String STEP_7_Close_out_Tab()
    {
        return "//div[text()='STEP 7: Close-out']";
    }

    public static String visible_drop_down_transition(String data)
    {
        return "//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]";
    }

    public static String Comments_on_outcome()
    {
        return "//div[@id='control_CA321829-EA9F-434D-949F-4D2829CE4381']//textarea";
    }

    public static String Outcome_received_by()
    {
        return "//div[@id='control_B483F7B8-37D4-4BEB-9767-B9CF75AA09AB']//ul";
    }

    public static String Accept()
    {
        return "//div[@id='control_9FA5EE26-05A3-476E-9424-F30CDF10EDDA']//ul";
    }

    public static String Return()
    {
        return "//div[@id='control_387C689E-ACB1-48CB-B8C1-3F06E18DA104']//ul";
    }

    public static String Outcome_data()
    {
        return "//div[@id='control_BE782700-AE1F-472C-8CD8-BFB828B592DD']//input";
    }

    public static String RelateEngagement_Record(String data)
    {
        return "(//div[@id='control_7A3B98E1-0126-40CA-B161-28ED379F71F5']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String RelatedEngagement()
    {
        return "(//div[text()='Related Engagement'])[1]";
    }

    public static String loadingData()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }

    public static String loading()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading...']";
    }

    public static String AudienceTypeSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String AudienceType_Dropdown()
    {
        return "//div[@id='control_74C6DEB4-AE07-4476-AF24-753B7187F94A']";
    }

    public static String EngagementStatus_Dropdown()
    {
        return "(//td[text()='Engagement status']//..//td[@class='sel']//div)[1]";
    }

    public static String EngagementStatus_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String GrievanceRecord(String data)
    {
        return "(//div[@id='control_378A887E-105F-4423-AABC-0F15AB543FF5']//div//table)[3] //div[text()='" + data + "']";
    }

    public static String GrievanceRecord()
    {
        return "((//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div//table)[3] //div[text()='Global Company -> South Africa -> Victory Site'])[1]";
    }

    public static String IndividualsTab()
    {
        return "//div[text()='Individuals']";
    }

    public static String RelatedGrievancePanel()
    {
        return "//span[text()='Related Grievances']";
    }

    public static String SaveSupportingDocuments_SaveBtn()
    {
        return "//div[@id='control_F33B6F6E-13E8-4ABC-8364-61C5086C73A3']";
    }

    public static String SearchButton()
    {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[text()='Search'])[1]";
    }

    public static String SearchButton1()
    {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[text()='Search'])[2]";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String Global_Company_Drop_Down()
    {
        return "(//li[@title='Global Company']//i)[1]";
    }

    public static String SaveToContinue_SaveBtn()
    {
        return "//div[@id='control_ED751D41-E936-4042-8E41-FB030CA70377']//div[text()='Save to continue']";
    }

    public static String searchBtn1()
    {
        return "(//div[@id='control_50E85B7D-AC6F-4FBD-9745-B4F4583650CD']//div[text()='Search'])[1]";
    }

    public static String InitiativesRecord()
    {
        return "//div[@id='control_BEEA2C5C-D12B-4585-BB9E-8A220737D2C3']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String initiatives_add_Btn()
    {
        return "//div[@id='control_6607FBD8-B7AF-4790-BF76-23119AE7ACA7']//div[text()='Add']";
    }

    public static String approvedBudget()
    {
        return "//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input[@type='number']";
    }

    public static String deliveryDate()
    {
        return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }

    public static String commencementDate()
    {
        return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }

    public static String socialInitiativesProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }

    public static String BusinessUnitTab()
    {
        return "//div[@id='control_6A2CB420-D98E-4181-9519-1658C80C4EE5']";
    }

    public static String TypeOfInitiative()
    {
        return "//div[@id='control_2C68539F-B3A7-4844-A919-CCA67BB70A53']";
    }

    public static String TypeOfInitiativeArrow()
    {
        return "//a[text()='Environmental Initiatives']/../i";
    }

    public static String TypeOfInitiativeSelect(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String addAnInitiativeBtn()
    {
        return "//div[@id='control_553E1A4E-09B8-4A64-8B9D-A631D30F836E']//div[text()='Add an initiative']";
    }

    public static String navigate_StakeholderIndividual()
    {
        return "//label[text()='Stakeholder Individual']";
    }

    public static String searchFieldIndividual()
    {
        return "(//div[@id='searchOptions']//td[@class='sel']//input[@class='txt border'])[1]";
    }

    public static String selectIndividualRecord(String data)
    {
        return "//div[text()='" + data + "']";
    }

    public static String si_processflow()
    {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }

    public static String individual_relationshipOwner()
    {
        return "//div[@id='control_F9D5D37A-1D64-4710-B6A3-BCF77F4D567C']";
    }

    public static String rightArrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String initiatives_Tab()
    {
        return "//div[text()='Initiatives']";
    }

    public static String responsiblePerson_DD()
    {
        return "//div[@id='control_F9C1A84D-4E33-45BC-9C36-2BF4E8BDBC38']";
    }

    public static String responsiblePerson_Select(String data)
    {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + data + "')]";
    }

    public static String socialInitiative_SaveBtn()
    {
        return "//div[@id='btnSave_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']//div[text()='Save']";
    }

    public static String stakeholderIndividualPanel()
    {
        return "//span[text()='Stakeholder Individual Beneficiaries']";
    }

    public static String beneficiries_Tab()
    {
        return "//div[text()='Beneficiaries']";
    }

    public static String individualBeneficiary()
    {
        return "//div[@id='control_2BA9C0DA-BB2A-4D66-9800-2DB2BD90F7EA']";
    }

    public static String entityBeneficiary_Select(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String navigate_complaintsGrievances()
    {
        return "//label[text()='Complaints & Grievances']";
    }

    public static String individualSave()
    {
        return "//div[@id='control_6607FBD8-B7AF-4790-BF76-23119AE7ACA7']//div[text()='Save']";
    }

    public static String searchBtn2()
    {
        return "(//div[@id='searchOptions']//div[text()='Search'])[2]";
    }

    public static String viewFullReportsIcon()
    {
        return "//span[@title='Full report ']";
    }

    public static String reportsBtn()
    {
        return "//div[@id='btnReports']";
    }

    public static String viewReportsIcon()
    {
        return "//span[@title='View report ']";
    }

    public static String continueBtn()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static void setGrievanceTitle(String value)
    {
        grievanceTitle = value;
    }

    public static String navigate_commitments()
    {
        return "//label[text()='Commitments']";
    }

    public static String navigate_socialSustainability()
    {
        return "//label[text()='Social Sustainability']";
    }

    public static String isoHome()
    {
        return "//div[@class='iso header item brand']";
    }

    public static String navigate_socialInitiatives()
    {
        return "//label[text()='Social Initiatives']";
    }

    public static String getGrievanceTitle()
    {
        return grievanceTitle;
    }

    public static void setProjectTitle(String value)
    {
        projectTitle = value;
    }

    public static String getProjectTitle()
    {
        return projectTitle;
    }

    public static void setCommitmentTitle(String value)
    {
        commitmentTitle = value;
    }

    public static String getCommitmentTitle()
    {
        return commitmentTitle;
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String businessUnitTab()
    {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']";
    }

    public static String expandGlobalCompany()
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'Global Company')]/../i";
    }

    public static String expandSouthAfrica()
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'South Africa')]/../i";
    }

    public static String closeBusinessUnit()
    {
        return "(//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//span[2]//b[1])";
    }

    public static String projectLink()
    {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }

    public static String projectTab()
    {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }

    public static String anyProject(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String engagementDate()
    {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String navigate_Stakeholders()
    {
        return "//label[text()='Stakeholder Management']";
    }

    public static String navigate_Engagements()
    {
        return "//label[text()='Engagements']";
    }

    public static String SE_add()
    {
        return "//div[text()='Add']";
    }

    public static String process_flow()
    {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }

    public static String engagementTitle()
    {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }

    public static String businessUnit_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String business_UnitSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String projectSelct(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String engagementPurposeTab()
    {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']";
    }

    public static String anyEngagementPurpose(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String engagementMethodTab()
    {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }

    public static String engagementMethodArrow()
    {
        return "//a[text()='In-person engagements']/../i";
    }

    public static String anyEngagementMethod(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String confidentialCheckBox()
    {
        return "//div[@id='control_C108E1A8-9B60-4E8B-B85A-08E47E5C6A7D']/div[1]//div[1]";
    }

    public static String responsiblePersonTab()
    {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']";
    }

    public static String responsiblePersonSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String impactTypeSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String contactInquiry()
    {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']";
    }

    public static String contactInquirySelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String locationDD()
    {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']";
    }

    public static String location()
    {
        return "//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']";
    }

    public static String locationSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String relatedSaveBtn()
    {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[text()='Save']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String mapPanel()
    {
        return "//span[text()='Map']";
    }

    public static String si_locationmarker()
    {
        return "//div[@id='control_10D8EA72-0F58-41B9-92D1-D8583BC3ED96']//div[@class='icon location mark mapbox-icons']";
    }

    public static String engagementDescription()
    {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }

    public static String engagementPurpose()
    {
        return "(//div[@id='control_180C969D-A1C8-46FF-9F11-EA0457D2F762']//textarea)[1]";
    }

    public static String si_location()
    {
        return "Montana.PNG";
    }

    public static String latitudeField()
    {
        return "//div[@id='control_DAA4D814-C0FD-417C-A22F-79CADF81F81A']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String longitudeField()
    {
        return "//div[@id='control_A5D5CA3A-9002-4FF7-A8B2-EC09C0B951D0']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox()
    {
        return "//div[@id='control_1A5C5270-DE92-4E2A-BBEA-8082A03DDA43']//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String attendiesTab()
    {
        return "//div[text()='Attendees']";
    }

    public static String totalNonListedAttendies()
    {
        return "//div[@id='control_8A28B08F-2857-4EB7-833E-3C7929A500B7']//input[@type='number']";
    }

    public static String individualsTab()
    {
        return "//div[text()='Individuals']";
    }

    public static String individuals_add()
    {
        return "//div[@id='control_2E2CA02B-9570-4DC8-89A7-8EE214D2F06E']//div[@id='btnAddNew']";
    }

    public static String individual_AttendeeNameDD()
    {
        return "//div[@id='control_DA9B0184-991D-4267-B01B-36EF4792AF67']";
    }

    public static String attendeeNameSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String entitiesTab()
    {
        return "//div[text()='Entities']";
    }

    public static String entities_add()
    {
        return "//div[@id='control_FBBF4EE8-0B0B-43B6-9B13-642704FDA74B']//div[@id='btnAddNew']";
    }

    public static String entity_AttendeeNameDD()
    {
        return "//div[@id='control_BCCAA4BD-BD02-428C-A1E3-3BFE31AF66BF']";
    }

    public static String entity_AttendeeNameSelect(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String attendeeName_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String teamTab()
    {
        return "//div[text()='Team']";
    }

    public static String team_add()
    {
        return "//div[@id='control_5D1D4343-6774-40DF-AAA5-4BE09A85DDCE']//div[@id='btnAddNew']";
    }

    public static String team_AttendeeDD()
    {
        return "//div[@id='control_B5CF2925-9A5E-4F93-A5D4-462B76CFEBF9']";
    }

    public static String grievancesTab()
    {
        return "//div[text()='Grievances']";
    }

    public static String grievance_add()
    {
        return "//div[text()='Add']";
    }

    public static String createGrievancePanel()
    {
        return "//span[text()='Create Grievance']";
    }

    public static String grievanceProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String grievanceTitle()
    {
        return "//div[@id='control_2640F649-7C15-4A4A-A49D-46D00B350FC0']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String grievanceSummary()
    {
        return "//div[@id='control_051ED5F3-EF25-4BED-BA5A-12FE10DD3877']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String individualOrEntityDD()
    {
        return "//div[@id='control_51BF378D-98C2-47AD-AF1A-22AFCB271228']";
    }

    public static String individualOrEntitySelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String grievantNameDD()
    {
        return "//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']";
    }

    public static String grieventNameSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String dateOfOccurence()
    {
        return "//div[@id='control_7DE47606-5B15-404F-872C-238816F07FDD']//input";
    }

    public static String grievance_SaveBtn()
    {
        return "//div[@id='btnSave_form_D0E4A377-80F0-4BD8-91E7-9C0D6EE2E888']";
    }

    public static String closeBtn()
    {
        return "//div[@id='form_D0E4A377-80F0-4BD8-91E7-9C0D6EE2E888']/div[@class='navbar']/i[@class='close icon cross']";
    }

    public static String relatedGrievancesPanel()
    {
        return "//span[text()='Related Grievances']";
    }

    public static String relatedGrievanceProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String clickRelatedGrievance()
    {
        return "//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }

    public static String search1()
    {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Search')])[1]";
    }

    public static String search2()
    {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Search')])[2]";
    }

    public static String searchGrievanceTitle()
    {
        return "//div[@id='searchOptions']//tr[@module_definition_name='txb_Grievance_title']//input[@class='txt border']";
    }

    public static String searchBtn()
    {
        return "//div[@id='searchOptions']//div[contains(text(),'Search')]";
    }

    public static String clickGrievanceRecord()
    {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }

    public static String grievance_Process_flow()
    {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String actions_tab()
    {
        return "//div[text()='Actions']";
    }

    public static String Actions_add()
    {
        return "//div[@id='control_5C3A47DC-4956-4D6E-BF97-A3967E24667B']//div[text()='Add']";
    }

    public static String Actions_pf()
    {
        return "//div[@id='btnProcessFlow_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }

    public static String Actions_desc()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Actions_deptresp_dropdown()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }

    public static String Actions_deptresp_select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Actions_deptresp_select1(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Actions_respper_dropdown()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }

    public static String ActionsRespper_select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Actions_ddate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_save()
    {
        return "//div[@id='btnSave_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }

    public static String Actions_savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String actions_CloseBtn()
    {
        return "//div[@id='form_5110F491-CFCA-42F7-87A2-001109988E70']/div[@class='navbar']/i[@class='close icon cross']";
    }

    public static String clickEngagementRecord()
    {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }

    public static String engagementStatusDD()
    {
        return "//div[@id='control_C072FF42-2D88-4B84-866B-FE7E9436460F']";
    }

    public static String initiativesTab()
    {
        return "//div[text()='Initiatives']";
    }

    public static String createInitiativesPanel()
    {
        return "//span[text()='Create Initiative']";
    }

    public static String initiative_add()
    {
        return "//div[@id='control_B589B7AE-E477-4356-B348-38C28AC45FB8']//div[text()='Add']";
    }

    public static String project_Title()
    {
        return "//div[@id='control_2786D8FA-CB59-4542-B173-B5220F2FC786']//input";
    }

    public static String location_DD()
    {
        return "//div[@id='control_E89884D1-4640-4024-B57E-C1C78775432A']";
    }

    public static String location_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String sector_DD()
    {
        return "//div[@id='control_2D64B1F8-1C51-48FC-919F-8EB0A3E028A7']";
    }

    public static String initiative_SaveBtn()
    {
        return "//div[@id='control_B589B7AE-E477-4356-B348-38C28AC45FB8']//div[text()='Save']";
    }

    public static String socialInitiativesPanel()
    {
        return "//span[text()='Create Initiative']";
    }

    public static String searchProjectTitle()
    {
        return "//div[@id='searchOptions']//tr[@module_definition_name='txb_n1']//input[@class='txt border']";
    }

    public static String relatedInitiavesPanel()
    {
        return "//span[text()='Related Initiatives']";
    }

    public static String initiative_search1()
    {
        return "(//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Search')])[1]";
    }

    public static String initiative_search2()
    {
        return "(//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Search')])[2]";
    }

    public static String clickRelatedInitiative()
    {
        return "//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }

    public static String relatedInitiativeProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }

    public static String commitmentsTab()
    {
        return "//div[text()='Commitments']";
    }

    public static String createCommitmentPanel()
    {
        return "//span[text()='Create Commitment']";
    }

    public static String commitment_add()
    {
        return "//div[@id='control_23054DE1-A737-47BF-89FF-83818E445F9F']//div[text()='Add']";
    }

    public static String commitment_SaveBtn()
    {
        return "//div[@id='control_23054DE1-A737-47BF-89FF-83818E445F9F']//div[text()='Save']";
    }

    public static String relatedCommintmentsPanel()
    {
        return "//span[text()='Related Commitments']";
    }

    public static String commitments_search1()
    {
        return "(//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Search')])[1]";
    }

    public static String commitments_search2()
    {
        return "(//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Search')])[2]";
    }

    public static String clickRelatedCommitments()
    {
        return "//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }

    public static String relatedCommitmentProcess_flow()
    {
        return "//div[@id='btnProcessFlow_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    }

    public static String Victory_Site_drop_Down()
    {
        return "(//li[@title='Global Company']//i)[8]";
    }

    public static String South_Africa_Drop_Down()
    {
        return "(//li[@title='Global Company']//i)[4]";
    }

    public static String Typeofsearch()
    {
        return "(//input[@placeholder='Type to search'])[39]";
    }

    public static String Typeofsearch2()
    {
        return "(//input[@placeholder='Type to search'])[18]";
    }

    public static String individualOrEntitySelect_2(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Type_of_observation(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String impactType()
    {
        return "//div[@id='control_81A52C73-0966-436D-9973-25C81E5CA966']//li";
    }

    public static String BusinessUnit_Option1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    
    public static String BusinessUnit_Option2(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[2]";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String receptionDate()
    {
        return "//div[@id='control_61CCCAF9-261B-421E-9682-A1EACFB89ACB']//input";
    }

    public static String GrievanceLocationTab()
    {
        return "//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']";
    }

    public static String GrievanceSaveBtn()
    {
        return "//div[@id='btnSave_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }

    public static String receivedByDD()
    {
        return "//div[@id='control_7198DB52-AF78-41DB-ADE2-D8C9044868A6']";
    }

    public static String ResponsibilityDD()
    {
        return "//div[@id='control_F9C1A84D-4E33-45BC-9C36-2BF4E8BDBC38']";
    }

    public static String responsibilitySelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String grievantNameDD_2()
    {
        return "//div[@id='control_3E5174D9-094B-4422-8BC5-A0C3CB60B129']";
    }

    public static String fullName()
    {
        return "//div[@id='control_16CE7364-9A28-43D8-9EE9-D2609B77196D']";
    }

    public static void setFullName(String value)
    {
        fullName = value;
    }

    public static String getFullName()
    {
        return fullName;
    }

    //Grivances V5 Page Objets
    public static String AcceptRejectObjection_Dropdown()
    {
        return "//div[@id='control_9FA5EE26-05A3-476E-9424-F30CDF10EDDA']";
    }

    public static String AcceptRejectObjection_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String CloseOutCompleted_Dropdown()
    {
        return "//div[@id='control_CAF67324-9F1B-4A3D-91AF-05516EC6EF54']";
    }

    public static String CloseOutCompleted_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String CloseOut_Tab()
    {
        return "//div[text()='STEP 7: Close-out']";
    }

    public static String ActionPlanAccepted_Dropdown()
    {
        return "//div[@id='control_ACCD2E08-C794-4DD4-91DA-CF2CB86FE6B1']";
    }

    public static String ActionPlanAccepted_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String CommentsOnOutcome()
    {
        return "//div[@id='control_CA321829-EA9F-434D-949F-4D2829CE4381']//textarea";
    }

    public static String CommentsOn_CloseOut()
    {
        return "//div[@id='control_8E50592C-5FF4-4A2B-BD81-95789EC95993']//textarea";
    }

    public static String FinalResponse()
    {
        return "//div[@id='control_C48CEF1D-DB04-46D3-BF20-27EABCE0DFBA']//textarea";
    }

    public static String FinalResponseSent_Dropdown()
    {
        return "//div[@id='control_BE686628-A2A4-4878-B018-50BE822659F1']";
    }

    public static String FinalResponseSent_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String GrievanceHistory_Gridview()
    {
        return "//div[@id='control_4E255DA7-D659-473A-9460-99935C9D7BE0']//div[text()='Grievance History']";
    }

    public static String GrievanceHistory_Gridview_Record()
    {
        return "(//div[@id='control_4E255DA7-D659-473A-9460-99935C9D7BE0']//div//table)[3]";
    }

    public static String GrievanceHistory_Gridview_Search()
    {
        return "//div[@id='control_4E255DA7-D659-473A-9460-99935C9D7BE0']//span[@class='k-icon k-i-reload']";
    }

    public static String GrievanceOutcome_Dropdown()
    {
        return "//div[@id='control_CD1B93DB-29B8-4A0A-A476-DD5B0C2E6DF8']";
    }

    public static String GrievanceOutcome_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String OutcomeReceivedBy_Dropdown()
    {
        return "//div[@id='control_B483F7B8-37D4-4BEB-9767-B9CF75AA09AB']";
    }

    public static String OutcomeReceivedBy_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String OutcomeReceived_Dropdown()
    {
        return "//div[@id='control_387C689E-ACB1-48CB-B8C1-3F06E18DA104']";
    }

    public static String OutcomeReceived_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String ResponseDeadline()
    {
        return "//div[@id='control_3F42AA7B-6E60-45F2-BBA5-C928381EB1EB']//input";
    }

    public static String SaveAndContinue()
    {
        return "//div[@id='control_B16C0F2E-94CA-4080-855D-21196423458D']";
    }

    public static String SaveAndContinue_STEP6()
    {
        return "//div[@id='control_0B9218A7-6374-44CD-94CC-E63CC6F7FADA']//div[text()='Save and continue to STEP 6']";
    }

    public static String SaveAndContinue_STEP7()
    {
        return "//div[@id='control_22341C39-0E70-4522-B162-50914FE9F534']//div[text()='Save and continue to STEP 7']";
    }

    public static String right_Arrows()
    {
        return "(//div[@class='tabpanel_tab_content']/div)[2]";
    }
    public static String Grievances_Reception_Channel;

    public static String GrievanceHistory_Tab()
    {
        return "//div[text()='Grievance history']";
    }

    public static String ActionPlanCompleted_Dropdown()
    {
        return "//div[@id='control_1AD76720-37F6-4468-99CC-A244B4C8D143']";
    }

    public static String ActionPlanCompleted_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String ActualDate_ActionPlan()
    {
        return "//div[@id='control_CBB4845C-70BE-43FB-A455-CE5056EF5BCD']";
    }

    public static String FinalResponse_Tab()
    {
        return "//div[text()='STEP 5: Final Response']";
    }

    public static String Resolve_Tab()
    {
        return "//div[text()='STEP 6: Resolve']";
    }

    public static String GrievanceActionPlan_GridView(String recordId)
    {
        return "(//div[@id='control_DD77760C-75A8-4E82-9EBF-E0ABFFF8CDA0']//div//table)[3]//span[text()='" + recordId + "']";
    }

    public static String ReturnTOGrievance_Close()
    {
        return "(//div[@id='form_C5DA5299-6947-4026-8DBB-E6DCFB714378']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String SaveAndContinue_STEP5()
    {
        return "//div[@id='control_09651BE1-396E-4140-BE02-6074A7C72AAD']//div[text()='Save and continue to STEP 5']";
    }

    public static String getActionRecord()
    {
        return "(//div[@class='recnum']//div[@class='record'])[2]";
    }

    public static String ActionPlan_Tab()
    {
        return "//div[text()='STEP 4: Action Plan']";
    }

    public static String ActualInitialResponseDate()
    {
        return "//div[@id='control_FDC96D9B-3E49-43A6-9AE6-A79569C7E1DF']";
    }

    public static String ActualInvestigation_CloseDate()
    {
        return "//div[@id='control_37C0C189-8A9F-4A79-AD99-8016A89CD4B0']";
    }

    public static String AdditionalComments()
    {
        return "//div[@id='control_3206E672-723F-4C86-B585-DFB4DF06BA6B']//textarea";
    }

    public static String Business_Unit_Option1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Business_Unit_Option3(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String GrievanceActionPlan_Panel()
    {
        return "//div[@id='control_DD77760C-75A8-4E82-9EBF-E0ABFFF8CDA0']//div[text()='Grievance Action Plan']";
    }

    public static String GrievanceActionPlan_Panel_AddButton()
    {
        return "//div[@id='control_DD77760C-75A8-4E82-9EBF-E0ABFFF8CDA0']//div[text()='Add']";
    }

    public static String GrievanceActionPlan_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5DA5299-6947-4026-8DBB-E6DCFB714378']";
    }

    public static String GrievanceStatus()
    {
        return "//div[@id='control_5BC6C20E-21B7-420C-8DC2-7506D1B7E8A5']//li";
    }

    public static String HasInitialResponse_Dropdown()
    {
        return "//div[@id='control_25F5F919-A8D4-4C45-8FE0-CAD512ACF13C']";
    }

    public static String HasInitialResponse_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String InitialResponseDeadline()
    {
        return "//div[text()='Initial response deadline']";
    }

    public static String InvestigationCompleted_Dropdown()
    {
        return "//div[@id='control_273E42C8-AF77-428A-B733-F75196C2670B']";
    }

    public static String InvestigationCompleted_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String InvestigationResults()
    {
        return "//div[@id='control_0848F2A3-34FF-4159-A1B5-E6C3E72CE635']//textarea";
    }

    public static String Investigation_Tab()
    {
        return "//li[@id='tab_DF0D4FF9-D56B-49CF-896A-FB8DFA5874D6']//div[text()='STEP 3 : Investigation']";
    }

    public static String Map_Panel()
    {
        return "//div[@id='control_5DDAED56-FA16-4BDD-9A86-5E4EF00CBE1B']//span[text()='Map']";
    }

    public static String STEP_2_Initial_Response_Tab()
    {
        return "//div[text()='STEP 2: Initial Response']";
    }

    public static String SaveAndContinue_STEP3()
    {
        return "//div[@id='control_3803EBF2-F4E0-4116-934A-0864D8E3A2DB']//div[text()='Save and continue  to STEP 3']";
    }

    public static String SaveAndContinue_STEP4()
    {
        return "//div[@id='control_AA685012-5C37-47A8-A182-BF4EC21BAB7C']//div[text()='Save and continue to STEP 4']";
    }

    public static String Save_Button()
    {
        return "//div[@id='btnSave_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']//div[text()='Save']";
    }

    public static String Social_Sustainability()
    {
        return "//label[text()='Social Sustainability']";
    }

    public static String Grievances_Title_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Grievances_Relationship_Owner_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Grievances_Status_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Social_Sustainability_Heading()
    {
        return "//h1[text()='Social Sustainability']";
    }

    public static String Complaints_Grievances_Tab()
    {
        return "//label[text()='Complaints & Grievances']";
    }

    public static String Process_Flow()
    {
        return "//span[@class='img icon flow-tree icon-button']";
    }

    public static String Button_Add()
    {
        return "//div[text()='Add']";
    }

    public static String Grievance_title()
    {
        return "(//div[@id='control_2640F649-7C15-4A4A-A49D-46D00B350FC0']//input)[1]";
    }

    public static String Grievance_summary()
    {
        return "(//div[@id='control_051ED5F3-EF25-4BED-BA5A-12FE10DD3877']//textarea)[1]";
    }

    public static String Grievance_reference_code()
    {
        return "(//div[@id='control_C3FF2F54-1F11-440F-877C-E179E880EB5F']//input)[1]";
    }

    public static String Latitude()
    {
        return "(//div[@id='control_600F3FE7-F1C0-4081-8523-4CE27BB854FB']//input)[1]";
    }

    public static String Longitude()
    {
        return "(//div[@id='control_3952FD06-E9F5-4DB0-92C1-CCA7C1181454']//input)[1]";
    }

    public static String Grievances_Reception_Date()
    {
        return "(//div[@id='control_61CCCAF9-261B-421E-9682-A1EACFB89ACB']//input)[1]";
    }

    public static String Business_Unit()
    {
        return "(//div[@id='control_6A2CB420-D98E-4181-9519-1658C80C4EE5']//ul//li)";
    }

    public static String Business_Unit_Option(String string)
    {
        return "//a[text()='" + string + "']";
    }

    public static String Grievances_Received_by()
    {
        return "//div[@id='control_7198DB52-AF78-41DB-ADE2-D8C9044868A6']//ul//li";
    }

    public static String Grievances_Received_by_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Individual_Entity_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Individual_Entity()
    {
        return "//div[@id='control_51BF378D-98C2-47AD-AF1A-22AFCB271228']//ul//li";

    }

    public static String Grievances_Name_3()
    {
        return "//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Grievances_Name()
    {
        return "(//li[text()='Please select'])[8]";
    }

    public static String Grievances_Name_1()
    {
        return "//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Grievances_Name_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Grievances_Location()
    {
        return "//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']//ul//li";
    }

    public static String Grievances_Location_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Grievances_Responsibility()
    {
        return "//div[@id='control_F9C1A84D-4E33-45BC-9C36-2BF4E8BDBC38']//ul//li";
    }

    public static String Grievances_Responsibility_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Grievances_Reception_Channel_option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Grievances_Reception_Channel()
    {
        return "//div[@id='control_36ACD371-D11A-4D20-A89D-02CE2AB66EB9']//ul//li";
    }

    public static String failed()
    {
        return "(//div[@id='txtAlert'])[2]";
    }

    public static String inspection_Record_Saved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String Button_Save()
    {
        return "(//div[text()='Save'])[2]";
    }

    public static String Grievances_Name_Search()
    {
        return "(//ul[@class='jstree-container-ul jstree-children'])[4]//li[1]";
    }

    public static String Grievances_Location_Search(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Responsibility_Search(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Reception_Search(String text)
    {

        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Registration_label()
    {
        return "//div[text()='Registration has to be completed to move to STEP 2']";
    }

    public static String Registration_Complete()
    {
        return "//div[@id='control_93DE0394-7F2C-4178-9EA4-5DEF4EDA5922']//ul//li";
    }

    public static String Step_1_Registration()
    {
        return "//div[text()='STEP 1: Registration']";
    }

    public static String Registration_Complete_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

//    public static String saveWait2() {
//        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
//    }
    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String Grievances_Name_Search_2()
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'aa')]/..//i)[1]";
    }

    public static String Grievances_Name_Search_Item()
    {
        return "(//a[text()=',m ,'])[1]";
    }

    public static String Search_Text_Box()
    {
        return "(//i[@class='icon search'])[8]/..//input";
    }

    public static String Search_Text_Box_Results(String data)
    {
        return "return//a[text()='" + data + "']";
    }

    public static String Grievances_Name_Drop_Down(String data)
    {
        return "//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Button_Grievant_Not_Registered()
    {
        return "//div[@id='control_751D50A1-55CC-48B7-A987-DC6F27997E38']";
    }

    public static String Stakeholder_First_Name()
    {
        return "(//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']//input)[1]";
    }

    public static String Stakeholder_First_Name_()
    {
        return "(//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']//input)[1]";
    }

    public static String Stakeholder_Last_Name()
    {
        return "(//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']//input)[1]";
    }

    public static String Stakeholder_Known_as()
    {
        return "(//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']//input)[1]";
    }

    public static String Stakeholder_Known_as_2()
    {
        return "//div[@id='control_5126FC78-97E6-49AA-A6D0-327CD4FD2CC5']//input";
    }

    public static String Title()
    {
        return "(//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//ul//li)";
    }

    public static String Title_Options(String data)
    {
        return "return//a[text()='" + data + "']";
    }

    public static String Status()
    {
        return "(//div[@id='control_170F62D3-43C4-4548-A7C3-2E856B137AC8']//ul//li)";
    }

    public static String Status_Options(String data)
    {
        return "return//a[text()='" + data + "']";
    }

    public static String Stakeholder_Categories()
    {
        return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Entity_Categories()
    {
        return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Business_Units()
    {
        return "//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Impact_Types()
    {
        return "//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }

    public static String Grievances_Relationship_Owner()
    {
        return "//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }

    public static String Stakeholder_Relationship_Owner()
    {
        return "//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']//li";
    }

    public static String Grievances_Name_Enter()
    {
        return "(//div[contains(@class, 'transition visible')]//input[@class])[130]";
    }

    public static String Button_Grievant_Not_Registered_1()
    {
        return "//div[@id='control_B076A3D3-95D4-4A7C-A1BB-F2394441CF1B']//div[text()='If grievant not registered ADD new']";
    }

    public static String Button_Grievant_Not_Registered_2()
    {
        return "//div[text()='If entity not registered ADD new']";
    }

    public static String Grievances_Name_2()
    {
        return "(//li[text()='Please select'])[7]";
    }

    public static String Stakeholder_Entity_type()
    {
        return "//div[@id='control_121C3080-4A13-4AC2-8640-F2380C230CDC']";
    }

    public static String Stakeholder_Entity_type_Options()
    {
        return "//a[text()='test']";
    }

    public static String Stakeholder_Entity_Name()
    {
        return "//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Industry()
    {
        return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']//ul//li";
    }

    public static String Industry_Option(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Stakeholder_Entity_Description()
    {
        return "//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']//textarea";
    }

    public static String Stakeholder_Relationship_Owner_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Stakeholder_Entity_Business_Units()
    {
        return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Entity_Impact_Types()
    {
        return "//div[@id='control_962A59FE-78C1-4FCE-B40E-CAC2A8124522']//b[@original-title='Select all']";
    }
//    public static String Actions_desc(){
//        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
//    }
//    public static String Actions_deptresp_dropdown(){
//        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
//    }
//    public static String Actions_deptresp_select(String text){
//        return "(//a[text()='"+text+"'])[2]";
//    }
//    public static String Actions_respper_dropdown(){
//        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
//    }
//    public static String Actions_respper_select(String text){
//        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')])[2]";
//    }
//    public static String Actions_ddate(){
//        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
//    }
//    public static String Actions_save(){
//        return "//div[@id='btnSave_form_C5DA5299-6947-4026-8DBB-E6DCFB714378']";
//    }
//    public static String Actions_savewait(){
//        return "(//div[text()='Action Feedback'])[1]";
//    }

    public static String replicableUsers()
    {
        return "//div[@id='control_9FB64F38-240A-4D57-8EBF-202D8124CDEE']//div[1]//div";
    }

    public static String multipleUser_Select(String data)
    {
        return "(//a[contains(text(),'" + data + "')]/i)[1]";
    }

//    public static String supporting_tab() {
//        return "//li[@id='tab_C298F1A9-5ACB-4936-9F4B-2F21E3CDC3FE']";
//    }
    public static String linkADoc_buttonxpath()
    {
        return "(//b[@class='linkbox-link'])[2]";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String AddNewIndividual()
    {
        return "//div[@id='control_B076A3D3-95D4-4A7C-A1BB-F2394441CF1B']";
    }

    public static String AddNewEntity()
    {
        return "//div[@id='control_751D50A1-55CC-48B7-A987-DC6F27997E38']";
    }

    public static String EntityYypeSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String MembersTab()
    {
        return "//div[text()='Members']";
    }

    public static String AddBtn()
    {
        return "//div[@id='control_7016173C-BF8B-419B-8C46-D81EAA9AEC45']//div[text()='Add']";
    }

    public static String IndividualName()
    {
        return "//div[@id='control_623A6228-2A56-4954-91D6-D2E07B56E612']";
    }

    public static String Position()
    {
        return "//div[@id='control_8C0CA237-7663-407A-A383-71A76D340D97']";
    }

    public static String relatedStakeholser_Save()
    {
        return "//div[@id='control_7016173C-BF8B-419B-8C46-D81EAA9AEC45']//div[text()='Save']";
    }

    public static String RegistrationComplete_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String RegistrationComplete()
    {
        return "//div[@id='control_93DE0394-7F2C-4178-9EA4-5DEF4EDA5922']";
    }

    public static String initialResponseTab()
    {
        return "//div[text()='STEP 2: Initial Response']";
    }

    public static String initialResponseCompleted()
    {
        return "//div[@id='control_25F5F919-A8D4-4C45-8FE0-CAD512ACF13C']";
    }

    public static String additionalComments()
    {
        return "//div[@id='control_3206E672-723F-4C86-B585-DFB4DF06BA6B']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String investigationTab()
    {
        return "//div[text()='STEP 3 : Investigation']";
    }

    public static String investigationResults()
    {
        return "//div[@id='control_0848F2A3-34FF-4159-A1B5-E6C3E72CE635']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String investigationCompleted()
    {
        return "//div[@id='control_273E42C8-AF77-428A-B733-F75196C2670B']";
    }

    public static String linkbox2()
    {
        return "//div[@id='control_45A14C72-BB36-44CC-9B86-3484440588C6']//b[@original-title='Link to a document']";
    }

    public static String actionPlanTab()
    {
        return "//div[text()='STEP 4: Action Plan']";
    }

    public static String actionPlanAdd()
    {
        return "//div[@id='control_DD77760C-75A8-4E82-9EBF-E0ABFFF8CDA0']//div[text()='Add']";
    }

    public static String actionPlanDescription()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String actionPlanProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C5DA5299-6947-4026-8DBB-E6DCFB714378']";
    }

    public static String departmentResponsible()
    {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }

    public static String responsiblePerson()
    {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }

    public static String actionDueDate()
    {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input[@type='text']";
    }

    public static String actionPlanSaveBtn()
    {
        return "//div[@id='btnSave_form_C5DA5299-6947-4026-8DBB-E6DCFB714378']";
    }

    public static String linkbox3()
    {
        return "//div[@id='control_F335B583-BFA7-4D39-BA63-4DA25CD90131']//b[@original-title='Link to a document']";
    }

    public static String closeActionPlan()
    {
        return "(//div[@id='formWrapper_C5DA5299-6947-4026-8DBB-E6DCFB714378']/..//i[@class='close icon cross'])[1]";
    }

    public static String actionPlanCompeted()
    {
        return "//div[@id='control_1AD76720-37F6-4468-99CC-A244B4C8D143']";
    }

    public static String actionPlanCompeted_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String actionPlanResponsiblePerson_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[1]";
    }

    public static String actionPlanSupporting_tab()
    {
        return "//li[@id='tab_C298F1A9-5ACB-4936-9F4B-2F21E3CDC3FE']";
    }

    public static String GrievanceLatitude()
    {
        return "//div[@id='control_600F3FE7-F1C0-4081-8523-4CE27BB854FB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String GrievanceLongitude()
    {
        return "//div[@id='control_3952FD06-E9F5-4DB0-92C1-CCA7C1181454']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String finalRsponseTab()
    {
        return "//div[text()='STEP 5: Final Response']";
    }

    public static String finalResponse()
    {
        return "//div[@id='control_C48CEF1D-DB04-46D3-BF20-27EABCE0DFBA']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String actionPlanAccepted()
    {
        return "//div[@id='control_ACCD2E08-C794-4DD4-91DA-CF2CB86FE6B1']";
    }

    public static String finalResponseSent()
    {
        return "//div[@id='control_BE686628-A2A4-4878-B018-50BE822659F1']";
    }

    public static String right_arrow()
    {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }

    public static String Business_Unit_Gbobal_Company()
    {
        return "//div[contains(@class, 'transition visible')]//ul[@class='jstree-container-ul jstree-children']//li//a";
    }

    public static String visible_search_text_box()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String GrievanceHistory_Results()
    {
        return "//div[text()='No results returned']";
    }

    public static String Grievance_Action_Status()
    {
        return "//div[text()='To be initiated']";
    }

    public static String grievanceHistory()
    {
        return "//div[text()='Grievance History']";
    }

    public static String grievanceHistoryRecord()
    {
        return "//div[@id='control_4E255DA7-D659-473A-9460-99935C9D7BE0']//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }

    public static String grievanceHistory_ProcessFlow()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String left_arrow()
    {
        return"//div[@class='tabpanel_left_scroll icon chevron left']";
    }
    
    public static String office_URL() {
    return "https://www.office.com";
    }
    
    public static String office_signin() {
    return ".//a[contains(text(),'Sign in')]";
    }
    
    public static String office_email_id(){
    return ".//input[@type='email']";
    }
    
    public static String email_next_btn(){
    return ".//input[@value='Next']";
    }
    
    public static String office_password(){
    return "//input[@type='password']";
    }
    
    public static String office_signin_btn(){
    return "//input[@value='Sign in']";
    }
    
    public static String office_No_btn(){
    return "//input[@value='No']";
    }
    
    public static String outlook_icon(){
    return "//a[@id='ShellMail_link']";
    }
    
    public static String inbox_chevron_expand(){
    return "//i[@class='ms-Button-icon _1IolX_6rKX93IRLO1O_oCP _3o738zmfzs1fXK1kxpiX5 _1OPSsVxfk_GWTnwo-KIMYX flipForRtl icon-60']";
    }
    
    public static String system_mail_folder(){
    return "//span[text()='System Mail']";
    }
    
    public static String email_notification_Logged(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Grievances')][contains(text(),'has been assigned to you')])[1]";
    }
    
    public static String ActionsRecordNumber_xpath() {
    return "(//div[@id='form_DB1AFD7D-40B5-47B3-A866-45A3E2F86848']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String linkBackToRecord_Link(){
    return ".//a[@data-auth='NotApplicable']";
    }
    
    public static String Username() {
    return "//input[@id='txtUsername']";
    }

    public static String Password() {
    return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
    return "//div[@id='btnLoginSubmit']";
    }
}
